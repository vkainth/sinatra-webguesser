# Web Guesser

A simple guessing game, made in Ruby and Sinatra

## Introduction

Web Guesser is a number guessing game, much like Hello World. The computer
will randomly generate a number and the user needs to guess this number. Styling
is handled by the Bulma framework.

## Installation

- Install sinatra by running `gem install sinatra` from the terminal

- Download a copy of this repository

- Open the directory in your terminal

- Run `ruby web_guesser.rb` to start a development server
