require 'sinatra'
require 'sinatra/reloader'

SECRET_NUMBER = rand(101)

get '/' do
  guess = params["guess"]
  message = get_message(guess)
  erb :index, :locals => {:number => SECRET_NUMBER, :message => message}
end

def get_message(guess)
  guess = guess.to_i
  message = ''
  if guess
    if guess > SECRET_NUMBER
      message = 'Too high!'
      if (guess - SECRET_NUMBER) > 5
        message = 'Way too high!'
      end
    end
    if guess < SECRET_NUMBER
      message = 'Too low!'
      if (SECRET_NUMBER - guess) > 5
        message = 'Way too low!'
      end
    end
    if guess == SECRET_NUMBER
      message = "#{guess} is the correct answer!"
    end
  end
  message
end